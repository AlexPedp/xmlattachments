package com.progettoedp.parser;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.progettoedp.it.Logger;
import com.progettoedp.testo.Testo;


public class XMLParser {

	private static final Logger logger = Logger.getLogger(XMLParser.class);
	private static final String TAG = "XMLParser";

	private File file;
	 
	public XMLParser(File file){
		this.file = file;
	}

	public boolean run(){
		
		boolean pdfFound = false;
		
		logger.info(TAG, "Inizio Parser XML");

		try {

			InputStream inputStream = new FileInputStream(file.toString());
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputStream);

			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("Allegati");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				logger.info(TAG, "TAG Allegati: inizio elaborazione");

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element element = (Element) nNode;

					String nome =  element.getElementsByTagName("NomeAttachment").item(0).getTextContent();
//					String algoritmo =  element.getElementsByTagName("AlgoritmoCompressione").item(0).getTextContent();
					String formato =  element.getElementsByTagName("FormatoAttachment").item(0).getTextContent();
					String descrizione =  element.getElementsByTagName("DescrizioneAttachment").item(0).getTextContent();
					String attachment =  element.getElementsByTagName("Attachment").item(0).getTextContent();

					logger.info(TAG, "Nome Attachment :" + nome);
//					logger.info(TAG, "Algoritmo Attachment :" + algoritmo);
					logger.info(TAG, "Formato Attachment :" + formato);
					logger.info(TAG, "Descrizione Attachment :" + descrizione);
//					logger.info(TAG, "Attachment :" + attachment);


					Path pdfPath = Paths.get(attachment);
					String base64Binary = null;
					
//					String fileName = pdfPath.getFileName().toString();
//					String currentPath = System.getProperty("user.dir");
//					Path newPdfPath = Paths.get(currentPath + "\\" + fileName); 

					if (Files.exists(pdfPath)) {
						byte[] pdf = Files.readAllBytes(pdfPath);
						base64Binary = javax.xml.bind.DatatypeConverter.printBase64Binary(pdf); 
						logger.debug(TAG, "base64Binary: " + base64Binary);
						Node attachmentNode = element.getElementsByTagName("Attachment").item(0);
						attachmentNode.setTextContent(base64Binary);


						try {

							// write the content into xml file
							TransformerFactory transformerFactory = TransformerFactory.newInstance();
							Transformer transformer = transformerFactory.newTransformer();
							DOMSource source = new DOMSource(doc);
							StreamResult result = new StreamResult(new File(file.toString()));
							transformer.transform(source, result);
							pdfFound = true;
						

						} catch (Exception e) {
							logger.error(TAG, "Exception: " + e); 
						}

						// cancellazione PDF
//						newPdfPath.toFile().delete();
						if(!attachment.toUpperCase().contains(Testo.PRIVACY) && !attachment.toUpperCase().contains(Testo.CODICE_ETICO))
							pdfPath.toFile().delete();

					} else {
						pdfFound = false;
						logger.info(TAG, "Attachment : file non trovato");
					}

					
					logger.info(TAG, "TAG Allegati: fine elaborazione");

				}
			}
		} catch (Exception e) {
			logger.error(TAG, "Exception: " + e); 
		}
		
		return pdfFound;

	}

}
