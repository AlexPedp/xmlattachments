package com.progettoedp.logic;

import java.io.File;
import java.io.FileFilter;

public class FileExtensionFilter implements FileFilter {

    private final String[] validExtensions;

    public FileExtensionFilter(String... validExtensions) {
        this.validExtensions = validExtensions;
    }
 

	public boolean accept(File pathname) {
		 if (pathname.isDirectory()) {
	            return true;
	        }

	        String name = pathname.getName().toLowerCase();

	        for (String ext : validExtensions) {
	            if (name.endsWith(ext)) {
	                return true;
	            }
	        }

	        return false;
	}
}

