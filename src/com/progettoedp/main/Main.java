package com.progettoedp.main;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.progettoedp.ftp.FTPExecute;
import com.progettoedp.it.Logger;
import com.progettoedp.logic.FileExtensionFilter;
import com.progettoedp.parser.XMLParser;
import com.progettoedp.properties.AppProperties;
import com.progettoedp.testo.Testo;

 

public class Main {
	
	private static final Logger logger = Logger.getLogger(Main.class);
	private static final String TAG = "Main";
	
	private static final String SEMAFORO = "import.sem";
	
	public static void main(String[] args) throws Exception {
		
//		List<File> finder = new ArrayList<File>();
		
		File f = new File("application.properties");
		if(!f.exists()) { 
			AppProperties.getInstance().create();
		}
		
		Map<String, String> map = AppProperties.getInstance().load();
		
		String startFolder = (String) map.get("start_folder");
		String endFolder = (String) map.get("end_folder");
		String backup_folder = (String) map.get("backup_folder");
		Boolean subfolder = Boolean.parseBoolean((String) map.get("subfolder"));
		Boolean subfolder_ftp = Boolean.parseBoolean((String) map.get("subfolder_ftp"));
		String[] extensions = map.get("file_extension").toString().split("\\|");
		Boolean ftp = Boolean.parseBoolean((String) map.get("ftp"));
		String ip = (String) map.get("ip");
		String user = (String) map.get("user");
		String password = (String) map.get("password");
		
		File sem = new File(startFolder +  SEMAFORO);
		sem.createNewFile();
		
		File dir = new File(startFolder);

// PROBLEMA DA SANAGENS CHE NON LEGGEVA I FILES: TOGLIERE I COMMENTI SOLO IN QUEL CASO		
 		File finder[] = dir.listFiles(new FileExtensionFilter(extensions));
//		List<File> finder = (List<File>) FileUtils.listFiles(dir, extensions, true);
		
  		logger.info(TAG, "Cartella: " + dir.getAbsolutePath());
  		
  		if (ftp){
  			if (ip != null && user != null && password != null){
  				FTPExecute fe = new FTPExecute(ip, user, password, startFolder, subfolder_ftp);
  				finder = fe.run();
  			}
  		}
	 	
  		logger.info(TAG, "Counted Files: " + finder.length); 
  		int i = 0;
		for (File file: finder){
			
			try {
			
			i++;
			
			if ((file.getAbsolutePath()+"\\").equals(backup_folder)){
				logger.info(TAG, "File is directory: " + "|" + i + "|" + file.getAbsolutePath()); 
				continue;
			}
 
			logger.info(TAG, "File: " + "|" + i + "|" + file.getAbsolutePath()); 
			
			String parentPath = file.getParent() + "\\";
		 	if (parentPath.equals(backup_folder)){
		 		logger.info(TAG, "Backup folder: skipped");
		 		continue;
		 	}
			
			
			XMLParser xmlParser = new XMLParser(file);
			boolean found = xmlParser.run();
			if (!found) {
				logger.info(TAG, "Attachments not found");
				continue;
			}
			

			if (!getFileExtension(file).equals(Testo.XML)){
				continue;
			}
				
			
			Path src = Paths.get(file.getAbsolutePath());
			
			
			Path end = null;
			
			if (subfolder && !ftp){
				String vat = file.getParentFile().getName();
				end = Paths.get(endFolder +  "/" + vat + "/" + file.getName());
			} else {
 				end = Paths.get(endFolder + file.getName());
			}
			
 
			Path dst = Paths.get(backup_folder + file.getName());
			
			if (!Files.exists(Paths.get(backup_folder))) {
				try {
					Files.createDirectories(dst);
					logger.info(TAG, "Created directory backup"); 
				} catch (IOException e) {
					logger.error(TAG, "Exception: " + e); 
				}
			}
			
			try {
				Files.copy(src, end, StandardCopyOption.REPLACE_EXISTING);
				logger.info(TAG, "XML copied: " + end.toString()); 
				Files.copy(src, dst, StandardCopyOption.REPLACE_EXISTING);
				logger.info(TAG, "XML copied: " + dst.toString()); 
	 			file.delete();
	 			logger.info(TAG, "File deleted: " + file.toString());
			} catch (Exception e) {
				logger.error(TAG, "Exception: " + e); 
			}
			
			logger.info(TAG, "End Parser");
			
			} catch (Exception e) {
				logger.error(TAG, "Exception: " + e); 
			}
			
		}
		
		sem.delete();
		
		
	}
	
	public static List<Path> findAllFilesInDirectory(Path pathToDir) throws IOException { 
	    final List<Path> pathsToFiles = new ArrayList<>();

	    Files.walkFileTree(pathToDir, new SimpleFileVisitor<Path>() {
	        @Override
	        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
	            if (Files.isRegularFile(file)) {
	                pathsToFiles.add(file);
	            }
	            return FileVisitResult.CONTINUE;
	        }
	    });

	    return pathsToFiles;
	}
	
	public static String getFileExtension(File file) {
	    String name = file.getName();
	    int lastIndexOf = name.lastIndexOf(".");
	    if (lastIndexOf == -1) {
	        return "";  
	    }
	    return name.substring(lastIndexOf+1);
	}

}
