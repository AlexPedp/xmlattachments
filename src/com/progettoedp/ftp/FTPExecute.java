package com.progettoedp.ftp;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.net.PrintCommandListener;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

import com.progettoedp.it.Logger;

public class FTPExecute {

	private static final Logger logger = Logger.getLogger(FTPExecute.class);
	private static final String TAG = "FTPExecute";

	private List<File> xmlFiles = new ArrayList<File>();
	
	private FTPClient ftp = null;
	private String ip;
	private String user;
	private String pwd;
	private String path;
	private boolean subfolder;

	public FTPExecute(String ip, String user, String pwd, String path, boolean subfolder) throws Exception { 
		this.ip = ip;
		this.user = user;
		this.pwd = pwd;
		this.path = path;
		this.subfolder=subfolder;
	}

	public File[] run() throws IOException{
 
		ftp = new FTPClient();
		ftp.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out)));
		int reply;
		ftp.connect(ip);

		reply = ftp.getReplyCode();
		if (!FTPReply.isPositiveCompletion(reply)) {
			ftp.disconnect();
			logger.error(TAG, "Exception in connecting to FTP Server");
		}
		ftp.login(user, pwd);
		ftp.setFileType(FTP.BINARY_FILE_TYPE);
		ftp.enterLocalPassiveMode();


		String remoteDir = getPath(path);
		String localeDir = new File("").getAbsolutePath();

		if (!subfolder) {
			FTPFile[] files = ftp.listFiles(remoteDir);			

			for (FTPFile file : files) {
				String details = file.getName();
				String extension = FilenameUtils.getExtension(details);
				if (extension.toUpperCase().equals("XML")){
					xmlFiles.add(new File(localeDir + "/" + details));
					downloadFile(remoteDir + "/" + details, details);
					logger.info(TAG, "Lettura dati: " + details.toString());
				}
			}

		} else {
			xmlFiles = listFolder(ftp, remoteDir, localeDir);
		}

		return xmlFiles.toArray(new File[xmlFiles.size()]);
	}

	public void downloadFile(String remoteFilePath, String localFilePath) {
		try (FileOutputStream fos = new FileOutputStream(localFilePath)) {
			this.ftp.retrieveFile(remoteFilePath, fos);
			
			this.ftp.deleteFile(remoteFilePath);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private List<File> listFolder(FTPClient ftpClient, String remoteDir, String localeDir)  throws IOException	{

		logger.info(TAG, "Listing folder " + remoteDir);
		FTPFile[] remoteFiles = ftpClient.listFiles(remoteDir);

		for (FTPFile remoteFile : remoteFiles)   {

			if (!remoteFile.getName().equals(".") && !remoteFile.getName().equals("..") && !remoteFile.getName().toUpperCase().equals("BACKUP"))   {
				String remoteFilePath = remoteDir + "/" + remoteFile.getName();

				if (remoteFile.isDirectory())  {
					listFolder(ftpClient, remoteFilePath, localeDir);
				}
				else {

					String details = remoteFile.getName();
					String extension = FilenameUtils.getExtension(details);
					if (extension.toUpperCase().equals("XML")){
						xmlFiles.add(new File(localeDir + "/" + details));
						downloadFile(remoteDir + "/" + details, details);
						logger.info(TAG, "Lettura dati: " + details.toString());
					}
				}
			}
		}

		return xmlFiles;
	}

	public void disconnect() {
		if (this.ftp.isConnected()) {
			try {
				this.ftp.logout();
				this.ftp.disconnect();
			} catch (IOException f) {

			}
		}
	}

	public String getPath(String input){
		String output = input.replaceAll("(\\d+.){3}\\d+", "");
		output = output.trim().substring(2, output.length()-1);
		output = output.replaceAll("\\\\", "/");
		return output;
	}

}