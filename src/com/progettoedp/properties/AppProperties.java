package com.progettoedp.properties;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class AppProperties {
	
	private static AppProperties instance = null;
	
	 public static AppProperties getInstance() {
	      if(instance == null) {
	         instance = new AppProperties();
	      }
	      return instance;
	   }

	public void create(){
	 
		Properties prop = new Properties();
		OutputStream output = null;

		try {

			output = new FileOutputStream("application.properties");
			 
			prop.setProperty("start_folder", "\\\\192.168.1.2\\home\\fatture_pa\\pdf\\");
			prop.setProperty("end_folder", "\\\\192.168.1.2\\home\\fatture_pa\\");
			prop.setProperty("backup_folder", "\\\\192.168.1.2\\home\\fatture_pa\\pdf\\backup\\");
			prop.setProperty("file_extension", "xml");
			prop.setProperty("subfolder", "true");
			prop.setProperty("subfolder_ftp", "true");
			prop.setProperty("ftp", "false");
			prop.setProperty("ip", "192.168.1.1");
			prop.setProperty("user", "qsecofr");
			prop.setProperty("password", "qsecofr");
		 	
			// save properties to project root folder
			prop.store(output, null);

		} catch (IOException io) {
			io.printStackTrace();
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
	}
	
	public Map<String, String> load(){
		
		Map<String, String> map = new HashMap<String, String>();
		
		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream("application.properties");

			// load a properties file
			prop.load(input);
			
			for (final String name: prop.stringPropertyNames()) {
				map.put(name, prop.getProperty(name));
			}
			 

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return map;
	}
	
}
